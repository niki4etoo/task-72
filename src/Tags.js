
function Tags(tags){
	const tagsList = tags.tags.map((tag) => {
			return <div className="tag" key={tag.toString()}>#{tag}</div>
		});
		
	return (
		<div className="tags">
			{tagsList}
		</div>
	);
	
}

export default Tags;
