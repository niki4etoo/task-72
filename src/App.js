import "./App.css";
import Tags from "./Tags";

const listTags = ["boomdotdev", "task", "tags", "react"];

function App() {
  return (
    <div className="App">
      <section className="hero">
        <div className="hero-body">
          <p className="title">A React Task</p>
          <p className="subtitle">by Boom.Dev</p>
        </div>
      </section>
      <div className="container is-fullhd">
        <div className="notification">
			<Tags tags={listTags} />
        </div>
      </div>
    </div>
  );
}

export default App;
